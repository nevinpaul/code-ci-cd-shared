/**
 * Generates a greeting message.
 * 
 * @param {string} name - The name to greet.
 * @returns {Object} An object containing the greeting message.
 * @throws {Error} Throws an error if the name is not provided.
 */
function generateGreeting(name) {
  if (!name) {
    throw new Error('Name is required');
  }
  return { message: `Hello ${name}` };
}

export default generateGreeting;

